class CommentsController < ApplicationController
  require "open-uri"

  def index; end

  def new
    @comment = Comment.new
  end

  def create
    #ebay
    if current_user
      @id_ebay = ""
      if (comments_params["url"].include? "ebay.com/p/")
        @id_ebay = id_first_method(comments_params["url"])
      elsif (comments_params["url"].include? "ebay.com/urw/")
        @id_ebay = id_second_method(comments_params["url"])
      end
      @comments_count = put_pages_count(@id_ebay)/10


      i = 1
      @comment = ""
      @comment_title = ""
      url = Nokogiri::HTML(open("https://www.ebay.com/urw/shrek-love-/product-reviews/#{@id_ebay}?pgn=1"))
      puts(url.css("p.review-item-content"))
      @test = url.css("p.review-item-content").to_s
      if @test == ""
        redirect_to new_comment_path
      else
      while @comments_count + 1 != i
        puts(i)
        url = Nokogiri::HTML(open("https://www.ebay.com/urw/shrek-love-/product-reviews/#{@id_ebay}?pgn=#{i}"))
        puts ("https://www.ebay.com/urw/shrek-love-/product-reviews/#{@id_ebay}?pgn=#{i}")

        url.css("p.review-item-content").each do |link|
          @comment += link.content + " /*/ "
        end

        i += 1
      end


      $comment = @comment.split(" /*/ ")

      #analyzer
      analyzer = Sentimental.new
      analyzer.load_defaults
      analyzer.threshold = 0.1
      my_hash = []
      @sentiment = ""
      @score = ""
      $comment.each_with_index do |comment, i|
        @sentiment += (analyzer.sentiment comment).to_s + "/*/"
        @score += (analyzer.score comment).to_s + "/*/"
        sentiment = analyzer.sentiment comment
        score = analyzer.score comment
        my_hash << {comment: "#{comment}",score: "#{score}", sentiment: "#{sentiment}"}
      end
      $sentiment = @sentiment.split("/*/")
      puts ($sentiment)
      $score = @score.split("/*/")
      puts $score

      $comment_json = my_hash.to_json
      # puts $comment_json
      # $graph_array = []
      # $graph_array << {sentiment: "positive", count: "#{$sentiment.count("positive")}"}
      # $graph_array << {sentiment: "negative", count: "#{$sentiment.count("negative")}"}
      # $graph_array << {sentiment: "neutral", count: "#{$sentiment.count("neutral")}"}
      # $graph_array << {"positive": $sentiment.count("positive"), "negative": $sentiment.count("negative"), "neutral": $sentiment.count("neutral")}
      # $graph_array << {"negative": $sentiment.count("negative")}
      # $graph_array << {"neutral": $sentiment.count("neutral")}

      # puts $graph_array
      # if File.exist?('./file.json')
      #   File.delete("./file.json")
      # end
      # File.open('./file.json', 'w') do |f|
      #   f.write($comment_json.to_json)
      # end
      redirect_to comment_show_path
    end
    else
      redirect_to new_user_registration_path
    end

    def show_result
      $graph_array = []
      $graph_array << "positive"
      $graph_array << $sentiment.count("positive")
      $graph_array2 = []
      $graph_array2 << "negative"
      $graph_array2 << $sentiment.count("negative")
      $graph_array3 = []
      $graph_array3 << "neutral"
      $graph_array3 << $sentiment.count("neutral")
      #, "negative": $sentiment.count("negative"), "neutral": $sentiment.count("neutral")
      # $graph_array = $graph_array.to_h
      puts $graph_array

    end
    #parse rozetka
    # if current_user
      # if File.exist?('./file.txt')
      #   File.delete("./file.txt")
      # end
    #   @sum = 0
    #   file = File.new("./file.txt", "a:UTF-8")
    #   rate_word = Word.all
    #   id = comments_params["url"].match("p[0-9]+").to_s.gsub(/p/, '')
    #   puts(id)
    #   site = HTTParty.get("https://product-api.rozetka.com.ua/v3/comments/get?front-type=xl&goods=#{id}&page=1&sort=date&limit=10&lang=ru")
    #   site = site.parsed_response.to_json
    #   data_hash = JSON.parse(site)
    #   page = count = data_hash["data"]["pages"]["count"]
    #   i = 2
    #   while page + 1 != i
    #     site = HTTParty.get("https://product-api.rozetka.com.ua/v3/comments/get?front-type=xl&goods=#{id}&page=#{i}{}&sort=date&limit=10&lang=ru")
    #     site = site.parsed_response.to_json
    #     data_hash = JSON.parse(site)
    #     puts i
    #     data_hash["data"]["comments"].each do |comment|
    #       comment = comment['text'].downcase
    #       word = comment.split(" ")
    #       puts comment
    #       @sum = 0
    #       rate_word.each do |rate|
    #         if (comment.include? rate.word.to_s)
    #           if rate.rate
    #             @sum += 1
    #             puts rate.rate
    #           else
    #             puts rate.rate
    #             @sum -= 1
    #           end
    #         end
    #       end
    #       file.write("#{comment}, Оцінка:   #{@sum}   ||   \n\r")
    #     end
    #     i += 1
    #   end
    #   redirect_to open_files_index_path
    # else
    #   redirect_to new_user_registration_path
    # end
  end

  def put_pages_count(id)
    url = Nokogiri::HTML(open("https://www.ebay.com/urw/shrek-love-/product-reviews/#{id}?pgn=1"))
    comments_count = url.css("h2.reviews-section-title").text
    comments_count = comments_count.split(" ")
    comments_count = comments_count[0]
    comments_count = round(comments_count.to_i)
    return comments_count
  end

  def id_first_method(url)
    tmp = url.split("/")
    id_ebay = tmp[4].split("?")
    id_ebay = id_ebay[0]
    return id_ebay
  end

  def id_second_method(url)
    tmp = url.split("/")
    tmp = tmp[6].split("?")
    id_ebay = tmp[0]
    return id_ebay
  end

  def round(n)
    (n+10)/10*10
  end

  private

  def comments_params
    params.require(:comment).permit(:url)
  end
end
