json.extract! word, :id, :word, :rate, :created_at, :updated_at
json.url word_url(word, format: :json)
