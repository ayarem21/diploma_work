Rails.application.routes.draw do
  get 'open_files/index'
  resources :words
  devise_for :users
  root "comments#index"

  resources :comments
  resources :words, controller: 'words', as: 'admin_words'

  get '/comment_show', to: 'comments#show_result', as: 'comment_show'
end
