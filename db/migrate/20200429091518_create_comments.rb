class CreateComments < ActiveRecord::Migration[6.0]
  def change
    create_table :comments do |t|
      t.string :text
      t.string :nickname
      t.float :rate
      t.float :custom_rate

      t.timestamps
    end
  end
end
