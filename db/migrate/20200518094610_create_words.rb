class CreateWords < ActiveRecord::Migration[6.0]
  def change
    create_table :words do |t|
      t.string :word
      t.boolean :rate

      t.timestamps
    end
  end
end
